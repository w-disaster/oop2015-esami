package ex2015.a01a.e1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CoursesCalendarImpl implements CoursesCalendar {
	
	private Map<Pair<Day, Room>, List<Pair<String, Pair<Integer, Integer>>>> lessons;
	private List<Integer> possibleSlots;
	
	public CoursesCalendarImpl() {
		this.lessons = new HashMap<>();
		this.possibleSlots = List.of(9,10,11,12,14,15,16,17);
	}

	@Override
	public List<Integer> possibleSlots() {
		return this.possibleSlots;
	}

	@Override
	public void bookRoom(Day d, Room r, int start, int duration, String course) {
		Pair<Day, Room> key = new Pair<>(d, r);
		if(!this.lessons.containsKey(key)) {
			this.lessons.put(key, new ArrayList<Pair<String, Pair<Integer, Integer>>>());
			this.lessons.get(key).add(new Pair<String, Pair<Integer, Integer>>(course, new Pair<Integer, Integer>(start, duration)));	
		} else {
			if(this.lessons.get(key).stream()
			.flatMap(p -> Stream.iterate(p.getY().getX(), i -> i + 1).limit(p.getY().getY()))
			.filter(i -> Stream.iterate(start, s -> s + 1).filter(c -> this.possibleSlots.contains(c)).limit(duration).collect(Collectors.toList()).contains(i))
			.collect(Collectors.toList())
			.size() > 0){
				throw new IllegalStateException();
			}
			this.lessons.get(key).add(new Pair<String, Pair<Integer, Integer>>(course, new Pair<Integer, Integer>(start, duration)));
		}
	}

	@Override
	public Set<Pair<Integer, String>> dayRoomSlots(Day d, Room r) {
		if(this.lessons.containsKey(new Pair<>(d, r))) {
			return this.lessons.get(new Pair<Day, Room>(d, r)).stream()
					.flatMap(p -> Stream.iterate(new Pair<>(p.getY().getX(), p.getX()), i -> new Pair<>(i.getX() + 1, i.getY()))
							.filter(c -> this.possibleSlots.contains(c.getX()))
							.limit(p.getY().getY()))
					.collect(Collectors.toSet());
		}
		return new HashSet<>();
	}

	@Override
	public Map<Pair<Day, Room>, Set<Integer>> courseSlots(String course) {
		return this.lessons.entrySet().stream()
				.map(e -> new Pair<Pair<Day, Room>, Set<Integer>>(e.getKey(), e.getValue().stream()
						.filter(p -> p.getX().equals(course))
						.flatMap(p -> Stream.iterate(p.getY().getX(), i -> i + 1)
								.filter(i -> this.possibleSlots.contains(i))
								.limit(p.getY().getY()))
						.collect(Collectors.toSet())))
				.collect(Collectors.toMap(p -> p.getX(), p -> p.getY()));
	}

}
