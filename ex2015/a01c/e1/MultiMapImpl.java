package ex2015.a01c.e1;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class MultiMapImpl<K, V> implements MultiMap<K, V>{

	private Map<K, Set<V>> map;
	private Type type;
	
	public enum Type {
		STANDARD, UNMODIFIABLE;
	}

	public void setMap(Map<K, Set<V>> map) {
		this.map = map;
	}

	public MultiMapImpl(Map<K, Set<V>> map) {
		super();
		this.map = map;
		this.type = Type.UNMODIFIABLE;
	}
	
	public MultiMapImpl() {
		super();
		this.map = new HashMap<>();
		this.type = Type.STANDARD;
	}

	@Override
	public void add(K key, V value) {
		if(this.type.equals(Type.UNMODIFIABLE)) {
			throw new UnsupportedOperationException();
		}
		if(!this.map.containsKey(key)) {
			this.map.put(key, new HashSet<V>() {{
				this.add(value);
			}});
		}	
		this.map.get(key).add(value);
	}

	@Override
	public void add(K key, Iterable<V> values) {
		Iterator<V> it = values.iterator();
		while(it.hasNext()) {
			this.add(key, it.next());
		}
	}

	@Override
	public Set<V> get(K key) {
		Set<V> set = new HashSet<>();
		for(V v : this.map.get(key)){
			set.add(v);
		}
		return set;
	}

	@Override
	public Set<Pair<K, V>> entrySet() {
		return this.map.entrySet().stream()
				.flatMap(e -> e.getValue().stream().map(v -> new Pair<K, V>(e.getKey(), v)))
				.collect(Collectors.toSet());
	}

	@Override
	public Set<K> keys() {
		return this.map.keySet();
	}

	public Map<K, Set<V>> getMap() {
		return map;
	}
}
