package ex2015.a01c.e1;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class MapFactoryImpl implements MapFactory {

	@Override
	public <K, V> MultiMap<K, V> empty() {
		return new MultiMapImpl<K, V>();
	}
	
	public <K, V> Map<K, Set<V>> mapFromMultiMap(MultiMap<K, V> mmap){
		Map<K, Set<V>> map = new HashMap<>();
		for(Pair<K, V> e : mmap.entrySet()) {
			if(map.containsKey(e.getX())) {
				map.get(e.getX()).add(e.getY());
			} else {
				map.put(e.getX(), new HashSet<>() {{
					this.add(e.getY());
				}});
			}
		}
		return map;
	}
	
	@Override
	public <K, V> MultiMap<K, V> unmodifiable(MultiMap<K, V> mmap) {
		return new MultiMapImpl<K, V>(mapFromMultiMap(mmap));
	}

}
