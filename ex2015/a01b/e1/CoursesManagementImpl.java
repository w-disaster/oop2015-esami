package ex2015.a01b.e1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CoursesManagementImpl implements CoursesManagement {
	
	private Map<Integer, Map<Course, Pair<Optional<String>, List<String>>>> map;
	private List<Integer> years;
	
	public CoursesManagementImpl() {
		super();
		this.map = Stream.iterate(2000, i -> i + 1).map(i -> Map.entry(i, 
				Stream.of(Course.values())
				.map(c -> Map.entry(c, new Pair<Optional<String>, List<String>>(Optional.empty(), new ArrayList<>())))
				.collect(Collectors.toMap(e1 -> e1.getKey(), e1 -> e1.getValue()))))
				.limit(100)
				.collect(Collectors.toMap(e2 -> e2.getKey(), e2 -> e2.getValue()));
		this.years = new ArrayList<>();
	}

	@Override
	public void assignProfessor(Course c, int year, String professor) {
		if(this.map.get(year).get(c).getX().isPresent()) {
			throw new IllegalStateException();
		}
		if(!this.years.contains(year)){
			this.years.add(year);
		}
		this.map.get(year).replace(c, new Pair<>(Optional.of(professor), this.map.get(year).get(c).getY()));
	}

	@Override
	public void assignTutor(Course c, int year, String professor) {
		if(this.map.get(year).get(c).getY().size() == TUTORING_SLOTS) {
			throw new IllegalStateException();
		}
		if(!this.years.contains(year)){
			this.years.add(year);
		}
		this.map.get(year).get(c).getY().add(professor);
	}

	@Override
	public Set<Course> getUnassignedCourses(int year) {
		if(!this.years.contains(year)){
			this.years.add(year);
		}
		return this.map.get(year).entrySet().stream()
				.filter(e -> e.getValue().getX().isEmpty())
				.map(e -> e.getKey())
				.collect(Collectors.toSet());
	}

	@Override
	public Set<Pair<Integer, Course>> getCoursesByProf(String prof) {
		return this.map.entrySet().stream()
				.flatMap(e -> e.getValue().entrySet().stream()
						.filter(c -> c.getValue().getX().isPresent() && c.getValue().getX().get().equals(prof))
						.map(c -> new Pair<Integer, Course>(e.getKey(), c.getKey())))
				.collect(Collectors.toSet());
	}

	@Override
	public Map<Integer, Set<Pair<Course, Integer>>> getRemainingTutoringSlots() {
		return this.map.entrySet().stream()
				.filter(e -> this.years.contains(e.getKey()))
				.map(e -> Map.entry(e.getKey(), e.getValue().entrySet().stream()
						.map(c -> new Pair<Course, Integer>(c.getKey(), TUTORING_SLOTS - c.getValue().getY().size()))
						.collect(Collectors.toSet())))
				.collect(Collectors.toMap(fe -> fe.getKey(), fe -> fe.getValue()));
	}

	

}
